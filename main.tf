provider "azurerm" {
  
  version = "=2.20.0"
  subscription_id = "6d5ae732-b526-4f92-a94b-e7f460d3733a"
  client_id       = "935fb058-e6e1-4d48-9e21-47380b15b951"
  client_secret   = "3B51T_5j~40YpOd.rIrZg6~sdzX~dP.8VA"
  tenant_id       = "b066b0b3-57a6-451f-9111-12a529a39311"
  features {}
}
provider "random" {
}

resource "local_file" "filePath" {
    content     = "foo!"
    filename = "${path.module}/app.zip"
}

resource "azurerm_resource_group" "rg" {
  name     = "terraform-workshop"
  location = "westus"
}

resource "random_string" "sa_name" {
  length  = 10
  special = false
  upper   = false
}

resource "azurerm_storage_account" "sa" {
  name                     = "sa${random_string.sa_name.result}"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "asp" {
  name                = "asp"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  kind                = "FunctionApp"

  sku {
    tier = "Standard"
    size = "S1"
  }
}

resource "azurerm_function_app" "app" {
  name                      = "apptestforpradeep"
  location                  = azurerm_resource_group.rg.location
  resource_group_name       = azurerm_resource_group.rg.name
  app_service_plan_id       = azurerm_app_service_plan.asp.id
  storage_connection_string = azurerm_storage_account.sa.primary_connection_string
  version                   = "~2"
  app_settings = {
    FUNCTIONS_WORKER_RUNTIME     = "node"
    WEBSITE_NODE_DEFAULT_VERSION = "10.14.1"
    WEBSITE_RUN_FROM_PACKAGE     = "${path.module}/app.zip"
  }
}
